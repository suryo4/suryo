@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
  <section class="content">
    <div class="container-fluid">
      <div class="row">
      <div class="col-md-6">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <strong>{{ $message }}</strong>
        </div>
    @endif
  </div>
          <div class="col-md-12">
<h3>Daftar User</h3>
            <div class="box-body table-responsive">
              <table class="table table-stiped table-bordered table-penjualan" width="100%">
                  <thead >
                      <th width="5%">No</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Role</th>
                      <th>Aksi</th>
                  </thead>

                  <tbody>
           @foreach($user as $u)
            <tr>
              <td>{{$n++}}</td>
              <td>{{$u->name}}</td>
              <td>{{$u->email}}</td>
              <td>{{$u->role}}</td>
              <td><input type="hidden" id="idp" name="idp" value="{{$u->id}}"><a href="javascript:;" onclick="editpengguna({{$u->id}})" class="btn btn-info edit">
                        <i class="fa fa-pencil-alt"></i>
                      </a>
                      <a href="{{ route('pengguna-delete', $u->id) }}" class="btn btn-danger delete" onclick="return confirm('Apakah Anda yakin ingin menghapus data?')">
                        <i class="fa fa-trash"></i>
                      </a></td>

            </tr>
            @endforeach
                  </tbody>
              </table>
          </div>
<div id="editbaru">
<h3>Register Pengguna Baru</h3>
            <div class="card" align="left">
    <div class="row justify-content-left">
                <div class="card-body">
                    <form method="POST" action="{{'penggunainsert'}}">
                        @csrf
                        <div class="form-group row justify-content-left">
                            <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Role') }}</label>

                            <div class="col-md-6">
                  <select name="role" id="role" class="form-control">
                    <option disabled selected value="">Pilih Role</option>
                    <option value="admin">Admin</option>
                    <option value="kasir">Kasir</option>
                  </select>
                            </div>
                        </div>

                        <div class="form-group row justify-content-left">
                            <label for="name" class="col-md-4 col-form-label text-md-left">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-left">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-left">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        </div>
    </div>
</div>
</div>
</section> 
<script>
        $(document).ready(function() {
        
        });

    function editpengguna(id) {
console.log(id);
        $.ajax({
          type: "GET",
          headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
          url: "{{ url('editpengguna') }}/" + id,
          data: {id: id},
          success: function(data){
            $("#editbaru").html(data);
          }
        });
        }

</script> 
@endsection

@extends('layouts.admin')
@section('content')
    
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        {{-- <div class="col-md-1">
          <a href="{{ route('kategori-tambah') }}" class="btn btn-block btn-primary">Tambah</a>
        </div> --}}
        <div class="col-md-11">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">DataTables</li>

          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            {{-- <div class="card-header">
              <h3 class="card-title">DataTable with minimal features & hover style</h3>
            </div> --}}
            <div class="card card-success">
                <div class="card-header">
                  <h3 class="card-title">Edit Produk</h3>
                </div>
                <div class="card-body">
                
                <form method="post" action="{{ route('produk-update', $produk->id) }}" enctype="multipart/form-data">
                    @csrf
                    <label for="">Kode Produk</label>
                    <input class="form-control" type="text" name="product_code" placeholder="Kode Produk" value="Kode Saat ini adalah {{ $produk->product_code }}, akan berubah jika Kategori diubah" readonly="">
                    <br>
                    <label for="">Nama Produk</label>
                    <input class="form-control" type="text" name="product_name" placeholder="Nama Produk" value="{{ $produk->product_name }}">
                    <br>
                    <div class="form-group">
                        <label>Kategori</label>
                        <select class="custom-select" name="kategori">
                          <option disabled selected value="">Pilih</option>
                        @foreach ($kategori as $item)
                          <option value="{{ $item->id }}" @if($produk->category == $item->id) selected @endif>{{ $item->name_category }}</option>
                        @endforeach
                        </select>
                    </div>
                    <br>
                    <label for="">Harga Jual</label>
                    <input class="form-control" type="text" name="price" placeholder="Harga Jual" value="{{ $produk->price }}">
                    <br>
                    <label for="">Stok</label>
                    <input class="form-control" type="text" name="stock" placeholder="Stok" value="{{ $produk->stock }}">
                    {{-- <br>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Gambar</label>
                        <input type="file" name="gambar" class="form-control-file" id="exampleFormControlFile1">
                        <input type="hidden" name="gambar_lama" value="{{ $produk->image }}">
                    </div> --}}
                    <br>
                  <button class="btn btn-primary col-md-1" type="submit"><i class="fa fa-pencil-alt"></i> Edit</button>
                  <a href="{{ route('produk') }}" class="btn btn-danger col-md-1"><i class="fa fa-times"></i> Close</a>
                </form>
                </div>
                <!-- /.card-body -->
              </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection
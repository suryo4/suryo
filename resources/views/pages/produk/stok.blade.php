@extends('layouts.admin')
@section('content')
    
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        {{-- <div class="col-md-1">
          <a href="{{ route('kategori-tambah') }}" class="btn btn-block btn-primary">Tambah</a>
        </div> --}}
        <div class="col-md-11">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">DataTables</li>

          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            {{-- <div class="card-header">
              <h3 class="card-title">DataTable with minimal features & hover style</h3>
            </div> --}}
            <div class="card card-success">
                <div class="card-header">
                  <h3 class="card-title">Update Stok</h3>
                </div>
                <div class="card-body">
                
                <form method="post" action="{{ route('stok-edit') }}" enctype="multipart/form-data">
                    @csrf
                    {{-- <label for="">Kode Produk</label>
                    <input class="form-control" type="text" name="product_code" placeholder="Kode Produk" value="">
                    <br> --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Produk</label>
                                <select class="custom-select" name="product_id">
                                <option disabled selected value="">Pilih</option>
                                @foreach ($product as $item)
                                <option value="{{ $item->id }}">{{ $item->product_name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Stok</label>
                        <input class="form-control" type="number" name="stock" placeholder="Stok" value="">
                        </div>
                        <button class="btn btn-block btn-primary col-md-1" type="submit"><i class="fa fa-pencil-alt"></i> Edit</button>&nbsp;
                        <a href="{{ route('produk') }}" class="btn btn-danger col-md-1"><i class="fa fa-times"></i> Close</a>
                    </div>
                  
                </form>
                </div>
                <!-- /.card-body -->
              </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection
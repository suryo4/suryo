@extends('layouts.admin')
@section('content')
    
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">

<div class="row">
  <div class="col-md-6">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <strong>{{ $message }}</strong>
        </div>
    @endif
  </div>
</div>

      <div class="row mb-2">
        <div class="col-md-6">
          <a href="{{ route('produk-tambah') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
        </div>
        <div class="col-md-6">
        
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Produk</h3>
            </div>
            
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  {{-- <th>Id</th> --}}
                  <th>Kode Produk</th>
                  <th>Nama Produk</th>
                  <th>Kategori</th>
                  <th>Harga Jual</th>
                  <th>Stok</th>
                  {{-- <th>Gambar</th> --}}
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($produk as $item) 
                  <tr>
                    {{-- <td>{{ $item->id }}</td> --}}
                    <td>{{ $item->product_code }}</td>
                    <td>{{ $item->product_name }}</td>
                    <td>{{ $item->kategori->name_category }}</td>
                    <td>{{ $item->price }}</td>
                    <td>{{ $item->stock }}</td>
                    {{-- <td>{{ $item->image }}</td> --}}
                    <td>
                      <a href="{{ route('produk-edit', $item->id) }}" class="btn btn-info">
                        <i class="fa fa-pencil-alt"></i>
                      </a>
                      <a href="{{ route('produk-delete', $item->id) }}" class="btn btn-danger delete" onclick="return confirm('Apakah Anda yakin ingin menghapus data?')">
                        <i class="fa fa-trash"></i>
                      </button>
                    </td>
                  </tr>  
                  @endforeach
                  
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection
@extends('layouts.admin')
@section('title')
  Daftar Penjualan
@endsection

@section('breadcrumb')
  <li class="active">Daftar Penjualan</li>
@endsection

@section('content')
    
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">

<div class="row">
  <div class="col-md-6">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <strong>{{ $message }}</strong>
        </div>
    @endif
  </div>
</div>

      <div class="row mb-2">
        {{-- <div class="col-md-2">
          <a href="{{ route('member-tambah') }}" class="btn btn-block btn-primary">Tambah</a>
        </div>
        <div class="col-md-2">
          <a href="" class="btn btn-block btn-success">Cetak Kartu</a>
        </div> --}}
        <div class="col-md-8">
          <ol class="breadcrumb float-sm-right">

          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-8">
            <form method="POST" action="{{'simpandt'}}">
        @csrf
            <table class="table">
              
              <tbody>
                                <tr>
                  <td>
                <div class="form-group">
                    <label for="">Nomor Transaksi</label>
                  </td>
                  <td></td>
                  <td align="center">
                  <input id="nonota" name="nonota" class="form-control" type="hidden" value="{{$nonota}}">
                  <label><h2>{{$nonota}}</h2></label>
                </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label for="">Pilih Produk</label>
                  </td>
                  <td>

                  </td>

       
              
    <td class="col-md-6">
          <div class="form-group">
            <div class="row">
              <div class="col-10">
              <input id="number" name="number" type="text" list="browser" class="form-control" placeholder="">
              <datalist id="browser">
                @foreach($produk as $p)
                  <option data-customvalue="{{$p->product_name}}" data-id="{{$p->id}}" data-satuan="{{$p->product_code}}" data-harga="{{$p->price}}" value="{{$p->product_name}}">{{$p->product_code}}|{{$p->product_name}}</option>
                  @endforeach
              </datalist>
              </div>

              <div class="col-2">
              <span id="error" class="text-danger"></span>
              <a href="javascript:;" onclick="addpenunjang()" type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-plus"></i></a>
              </div>
              </div>
              </div>
                  </td>
                  
                </tr>
              </tbody>
            </table>
            {{-- <div class="form-group">
              <label for="">Kode Produk</label>
              <input class="form-control" type="text" name="kode" placeholder="" value="">
              <a href="" class="btn btn-info">
                <i class="fa fa-search"></i>
              </a>
            </div> --}}
            
            <div class="box-body table-responsive">
              <table class="table table-stiped table-bordered table-penjualan">
                  <thead>
                      <th width="5%">No</th>
                      <th>Kode Produk</th>
                      <th>Nama Produk</th>
                      <th>Harga</th>
                      <th>Jumlah</th>
                      <th>Sub Total</th>
                      <th>Aksi</th>
                      {{-- <th width="15%"><i class="fa fa-cog"></i></th> --}}
                  </thead>
            <!--       <input type="text" name="tes" id="cek"> <input id="preview_input"> -->
                  <tbody id="dynamicTable">

                  </tbody>
              </table>
          </div>

                                
          </div>
          <div class="col-md-4">
          <table class="table">
            <tbody>

              <tr>
                <td>
                  <label for="">Metode Pembayaran</label>
                </td>
                <td>
    <div class="form-group">
      <div class="row">
        <div class="col-9">
      
 {{--<input id="member" name="member" type="text" list="anggota" class="form-control">--}}
<datalist id="anggota">
   @foreach($member as $m)
    <option data-customvalue="{{$m->name}}" data-diskon="{{$diskon}}" data-code="{{$m->code}}" data-id="{{$m->id}}" value="{{$m->name}}">{{$m->code}}|{{$m->name}}</option>
    @endforeach
</datalist>
</div>
<div class="col-2">
        {{--<span id="error" class="text-danger"></span>
        <a href="javascript:;" onclick="addmember()" type="button" class="btn btn-success"><i class="fa fa-plus"></i></a>--}}
<span id="error" class="text-danger"></span>
        <a href="javascript:;" type="button" class="btn btn-success"><i class="fa fa-dollar-sign"></i></a>
        </div>
        </div>
    </div>
                </td>
                <td>
                  
                </td>
              </tr>
              <tr>
                <td>
                  <label for="">Diskon</label>
                </td>
                <td id="memberTable">
                  <input id="mem_id" name="mem_id" type="hidden">
                  <input id="mem_kode" name="mem_kode" type="hidden">
                  <input id="disc" name="disc" type="hidden">
                </td>
                <td></td>
              </tr>
              <tr>
                <td>
                  <label for="">Bayar Via</label>
                </td>
                <td>
                  <select name="via" id="via" class="form-control">
                    <option disabled selected value="">Pilih</option>
                    <option value="cash">Tunai</option>
                    <option value="debit">Non-tunai</option>
                  </select>
                </td>
                <td></td>
              </tr>
              <tr>
                <td>
                  <select name="bank" id="bank" class="form-control">
                    <option disabled selected value="">Pilih</option>
                    <option value="BCA">BCA</option>
                    <option value="BRI">BRI</option>
                    <option value="MANDIRI">Mandiri</option>
                  </select>
                </td>
                <td>
                  <input class="form-control" type="text" id="debit" name="nomor_bank" placeholder="Nomor Debit" value="">
                </td>
                <td></td>
              </tr>
              <tr>
                <td>
                  <label for="">Diterima</label>
                </td>
                <td>
                  <input class="form-control" type="number" id="diterima" name="terima" placeholder="" value="0">
                </td>
                <td></td>
              </tr>
              <tr>
                <td>
                  <label for="">Kembali</label>
                </td>
                <td>
                  <input class="form-control" type="number" id="change" name="kembali" placeholder="" value="0">
                </td>
                <td></td>
              </tr>

            </tbody>
          </table>
      </div>

    </div>
  <div class="row">
          <div class="col-md-9">
            <table class="table">
              <tbody>
                
                <tr>
                  <td class="col-md-3">
                    <label for="">Total Transaksi</label>
                  </td>
                  <td class="col-md-6">
                    <label class="form-control" id="TotalAll" name="totalAll"></label>
                    <input class="form-control" id="Totalbayar" name="totalbayar" type="hidden"></input>
                    <input class="form-control" id="qtytot" name="qtytot" type="hidden"></input>
                    <input class="form-control" id="nomtot" name="nomtot" type="hidden"></input>
                  </td>
                  <td class="col-md-3">
                   <button class="btn btn-primary" type="submit">
                    <i class="fa fa-save"></i> Simpan Transaksi</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </form>
        </div>
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<script type="text/javascript"> 
    var i = 0;
    var a = 0;
    var n = 1;
    function addpenunjang() {
        var values = $('#member').val();
        var value = $('#number').val();
        var code =$('#dv').val();
        var diskon =$('#anggota [value="' + values + '"]').data('diskon');
        var pen= $("#penunjang option:selected").html();
        var satuan =$('#browser [value="' + value + '"]').data('satuan');
        var harga =$('#browser [value="' + value + '"]').data('harga');
        var penid =$('#browser [value="' + value + '"]').data('customvalue');
        var id =$('#browser [value="' + value + '"]').data('id');
        console.log(code);
        if (penid != null) {

        $("#dynamicTable").append('<tr><td>'+n+'<input name="p_id[]" type="hidden" value="'+id+'"></input></td><td>'+satuan+'<input name="p_kode[]" type="hidden" value="'+satuan+'"></input></td><td>'+penid+'<input name="p_nama[]" type="hidden" value="'+penid+'"></input></td><td width="10%"><input name="price[]" value="'+harga+'" type="hidden">'+harga+'</td><td><input class="qty" type="number" min="1" max="" id="qty" name="qty[]" value="1" placeholder="Qty"></td><td><input type="number" value="'+harga+'" class="totalprice" name="total[]" placeholder="Total" readonly/></td><td><button type="button" class="btn btn-danger remove-pen">Hapus</button></td></tr>');
        }
        ++i;
        ++n;
        $('#number').val('');
        $('#member').val('');
if(code){
    var TotalValue = 0;
    $('.totalprice').each(function() {
       TotalValue += parseFloat($(this).val());
      });
    if(diskon){
    var disc=TotalValue*diskon/100;
    var Totaldisc=TotalValue-disc;
  }else{
    var Totaldisc=TotalValue;
  }
   $('#TotalAll').text(Totaldisc);
   $('#Totalbayar').val(Totaldisc);
   $('#nomtot').val(TotalValue);
  
     
}else{
    var TotalValue = 0;
    $('.totalprice').each(function() {
       TotalValue += parseFloat($(this).val());
      });
   $('#TotalAll').text(TotalValue);
   $('#Totalbayar').val(TotalValue);
  $('#nomtot').val(TotalValue);
 }
      var qt = 0;
      $('.qty').each(function() {
       qt += parseFloat($(this).val());
      });
        $('#qtytot').val(qt);
        console.log(qt);
}; 
    function addmember() {
        $('.dis').remove();
        var value = $('#member').val();
        var mem_id =$('#anggota [value="' + value + '"]').data('id');
        var nama =$('#anggota [value="' + value + '"]').data('customvalue');
        var code =$('#anggota [value="' + value + '"]').data('code');
        var diskon =$('#anggota [value="' + value + '"]').data('diskon');

        if (nama != null) {
        $("#memberTable").append('<tr class="dis"><td><input id="dv" value='+diskon+'% readonly>'+nama+'||'+code+'</input></td><td><button type="button" class="btn btn-danger remove-mem"><i class="fa fa-trash"></i></button></td></tr>');
        $('#mem_id').val(mem_id);
        $('#mem_kode').val(code);
        $('#disc').val(diskon);
        }
        $('#member').val('');

      var TotalValue = 0;
    $('.totalprice').each(function() {
       TotalValue += parseFloat($(this).val());
      });
    var disc=TotalValue*diskon/100;
    var Totaldisc=TotalValue-disc;
   $('#TotalAll').text(Totaldisc);
   $('#Totalbayar').val(Totaldisc);
}; 
  jQuery(document).on("change ,  keyup" , "input[name='qty[]'] , input[name='price[]']" ,function(){
     var parent_element = jQuery(this).closest("tr");
     var qty = jQuery(parent_element).find("input[name='qty[]']").val();
     var price = jQuery(parent_element).find("input[name='price[]']").val();
     var value = $('#member').val();
     var diskon =$('#anggota [value="' + value + '"]').data('diskon');
     if( qty.trim() != "" && price.trim() != "")
      {
        jQuery(parent_element).find("input[name='total[]']").val( parseFloat(qty) *parseFloat(price) );
    var TotalValue = 0;
    $('.totalprice').each(function() {
       TotalValue += parseFloat($(this).val());
      });
     var qt = 0;
        $('.qty').each(function() {
       qt += parseFloat($(this).val());
      });
        $('#qtytot').val(qt);
    if(diskon){
    var disc=TotalValue*diskon/100;
    var Totaldisc=TotalValue-disc;
  }else{
    var Totaldisc=TotalValue;
  }
   $('#TotalAll').text(Totaldisc);
   $('#Totalbayar').val(Totaldisc);
  $('#nomtot').val(TotalValue);

      }
      else
      {
        jQuery(parent_element).find("input[name='total[]']").val("");
      }

  });



$(document).on('click', '.remove-pen', function(){ 
  var value = $('#member').val();
  var diskon =$('#anggota [value="' + value + '"]').data('diskon'); 
         $(this).parents('tr').remove();
               var TotalValue = 0;
    $('.totalprice').each(function() {
       TotalValue += parseFloat($(this).val());
      });
    var disc=TotalValue*diskon/100;
    var Totaldisc=TotalValue-disc;
   $('#TotalAll').text(Totaldisc);
   $('#Totalbayar').val(Totaldisc);
  $('#nomtot').val(TotalValue);
     var qt = 0;
    $('.qty').each(function() {
       qt += parseFloat($(this).val());
      });
        $('#qtytot').val(qt);
});

$(document).on('click', '.remove-mem', function(){  
         $('.dis').remove();
               var TotalValue = 0;
    $('.totalprice').each(function() {
       TotalValue += parseFloat($(this).val());
      });
   $('#TotalAll').text(TotalValue);
    $('#Totalbayar').val(TotalValue);
});


  $(function() {
     $("#via").change(function (){
var via =$("#via option:selected").val();
var ttb= $('#Totalbayar').val();
if(via=='cash'){
$("#diterima").val(0);
$("#bank").prop('disabled', true);
$("#debit").prop('disabled', true);
$("#diterima").prop('readonly', false);
$("#change").prop('readonly', false);
}else{
  $("#bank").prop('disabled', false);
$("#debit").prop('disabled', false);
$("#diterima").prop('readonly', true);
$("#diterima").val(ttb);
$("#change").val(0);
$("#change").prop('readonly', true);
}
     });
  });

  $('#diterima').on('keyup', function(){
    var dt = parseFloat($(this).val());
    var ttl = parseFloat($('#TotalAll').text());
    var trm = dt - ttl;
    $('#change').val(trm);
  });

</script>
@endsection


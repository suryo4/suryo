@extends('layouts.admin')
@section('title')
  Daftar Penjualan
@endsection

@section('breadcrumb')
  <li class="active">Daftar Penjualan</li>
@endsection

@section('content')
    
<div class="content-wrapper">
  <section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
 
            
            <div class="box-body table-responsive">
              <table class="table table-stiped table-bordered table-penjualan" width="100%">
                  <thead >
                      <th width="5%">No</th>
                      <th>No Penjualan</th>
                      <th>Jenis Transaksi</th>
                      <th>Diskon</th>
                      <th>Harga Bruto</th>
                      <th>Harga Netto</th>
                      <th>Total Retur</th>
                      <th>Total Penjualan</th>
                      <th>Status</td>

                      <th>Aksi</th>
                  </thead>

                  <tbody>
           @foreach($penjualan as $pj)
            <tr>
              <td>{{$n++}}</td>
              <td>{{$pj->nonota}}</td>
              <td>{{$pj->via}}</td>
              <td>{{$pj->diskon}}</td>
              <td>{{$pj->harga_bruto}}</td>
              <td>{{$pj->harga_netto}}</td>
              <td>{{$pj->retur}}</td>
              <td>{{$pj->total}}</td>
              <td><?php 
                  if($pj->retur!=0){
                    echo "Retur";
                  }else{
                    echo "Selesai";
                  }
              ?></td>
              <td><a class="btn btn-primary" href="{{'cetak'}}/{{$pj->id}}">Cetak</a></td>

            </tr>
            @endforeach
                  </tbody>
              </table>
          </div>

        </div>
    </div>
   </div>
  </section>
  <!-- /.content -->
</div>
@endsection


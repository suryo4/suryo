@extends('layouts.admin')
@section('content')
    
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">

<div class="row">
  <div class="col-md-6">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <strong>{{ $message }}</strong>
        </div>
    @endif
  </div>
</div>
      <div class="row mb-2">
        <div class="col-md-10">
          <ol class="breadcrumb float-sm-right">
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Pengaturan</h3>
            </div>
          
              <form method="post" action="{{ route('setting-simpan') }}">
                @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Nama Perusahaan</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="nama_perusahaan" value="{{$setting->nama_perusahaan}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">No Telepon</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="no_telepon" value="{{$setting->no_telepon}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Alamat</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="alamat" value="{{$setting->alamat}}">
                  </div>
                </div>
                <div class="form-group">
                  {{--<label for="" class="col-sm-2 control-label">Diskon Member (%)</label>--}}

                  <div class="col-sm-10">
                    <input type="hidden" class="form-control" name="diskon_member" value="{{$setting->diskon_member}}">
                  </div>
                </div>

              <div class="box-footer">
                <button class="btn btn-primary col-md-1" type="submit"><i class="fa fa-save"></i> Save</button>
                
              </div>
              <!-- /.box-footer -->
            </form>
    
           
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection
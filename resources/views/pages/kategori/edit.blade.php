@extends('layouts.admin')
@section('content')
    
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        {{-- <div class="col-md-1">
          <a href="{{ route('kategori-tambah') }}" class="btn btn-block btn-primary">Tambah</a>
        </div> --}}
        <div class="col-md-11">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">DataTables</li>

          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            {{-- <div class="card-header">
              <h3 class="card-title">DataTable with minimal features & hover style</h3>
            </div> --}}
            <div class="card card-success">
                <div class="card-header">
                  <h3 class="card-title">Edit Kategori</h3>
                </div>
                <div class="card-body">
                <form method="post" action="{{ route('kategori-update', $kategori->id) }}">
                    @csrf
                  <label for="">Kode Kategori</label>
                  <input class="form-control" type="text" name="kode_category" placeholder="Kode Kategori" value="{{ $kategori->kode }}">
                  <br>
                  <label for="">Nama Kategori</label>
                  <input class="form-control" type="text" name="name_category" placeholder="Nama Kategori" value="{{ $kategori->name_category }}">
                  <br>
                  <button class="btn btn-primary col-md-1" type="submit"><i class="fa fa-pencil-alt"></i> Edit</button>
                  <a href="{{ route('kategori') }}" class="btn btn-danger col-md-1"><i class="fa fa-times"></i> Close</a>
                </form>
                </div>
                <!-- /.card-body -->
              </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection
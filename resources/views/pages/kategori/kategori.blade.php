@extends('layouts.admin')
@section('content')
    
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">

<div class="row">
  <div class="col-md-6">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <strong>{{ $message }}</strong>
        </div>
    @endif
  </div>
</div>
      <div class="row mb-2">
        <div class="col-md-2">
          <a href="{{ route('kategori-tambah') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
        </div>
        <div class="col-md-10">
          <ol class="breadcrumb float-sm-right">
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="box">
            <div class="box-header">
            <div class="row">
                <div class="col-9">
                  <h3 class="box-title">Daftar Kategori</h3>
                </div>
                
                <div class="col-3">
                  <!-- Search input -->
                  <!-- <input type="text" class="form-control" name="search" id="search" placeholder="Search"> -->
                </div>
                
                <!-- <div id="search_list"></div> -->
              </div>
            </div>
            
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  {{-- <th>Id</th> --}}
                   <th>Kode</th>
                  <th>Kategori</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($kategori as $item) 
                  <tr>
                    {{-- <td>{{ $item->id }}</td> --}}
                    <td>{{ $item->kode }}</td>
                    <td>{{ $item->name_category }}</td>
                    <td>
                      <a href="{{ route('kategori-edit', $item->id) }}" class="btn btn-info">
                        <i class="fa fa-pencil-alt"></i>
                      </a>
                      <a href="{{ route('kategori-delete', $item->id) }}" class="btn btn-danger delete" onclick="return confirm('Apakah Anda yakin ingin menghapus data?')">
                        <i class="fa fa-trash"></i>
                      </a>
                    </td>
                  </tr>  
                  @endforeach

                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection
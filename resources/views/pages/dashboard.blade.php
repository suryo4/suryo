@extends('layouts.admin')
@section('content')
    

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">Home</a></li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div id="pemasukan_back">
          <!--   <h4 class="header-title">Grafik Penjualan</h4> -->
                <canvas id="myChart_back"></canvas> 
</div>
        <div class="row_back">
          <!-- Left col -->
          <section class="col-lg-7 connectedSortable_back">
            <!-- Custom tabs (Charts with tabs)-->
            
            <!-- /.card -->

            <!-- /.card -->
          </section>
          <!-- /.Left col -->
          
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
        integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
        integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
    </script>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

<!-- Chart JS -->
<script src="assets/libs/chart-js/Chart.bundle.min.js"></script>

<!-- Init js -->
<script src="assets/js/pages/chartjs.init.js"></script>
<script>
            
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['januari','february','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'],
    datasets: [{
        label: 'Penjualan',
        data: {!! json_encode($penjualan) !!},
         backgroundColor: [
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)',
          'rgba(67, 143, 108, 0.959)'
        ],
        borderColor: [
          '#2dcc9a',
          '#2dcc9a',
          '#2dcc9a',
          '#2dcc9a',
          '#2dcc9a',
          '#2dcc9a',
          '#2dcc9a',
          '#2dcc9a',
          '#2dcc9a',
          '#2dcc9a',
          '#2dcc9a',
          '#2dcc9a'
        ],
        borderWidth: 2
      },
      {
        label: 'Retur',
        data: {!! json_encode($retur) !!},
        backgroundColor: [
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)',
          'rgba(182, 189, 185, 0.959)'

        ],
        borderColor: [
          '#f7f7f799',
          '#f7f7f799',
          '#f7f7f799',
          '#f7f7f799',
          '#f7f7f799',
          '#f7f7f799',
          '#f7f7f799',
          '#f7f7f799',
          '#f7f7f799',
          '#f7f7f799',
          '#f7f7f799',
          '#f7f7f799'
        ],
       
        borderWidth: 2
      }
    ]
  },
  options: {
    scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
  }
});
</script>
  @endsection
  
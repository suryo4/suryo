@extends('layouts.admin')
@section('content')
    
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        {{-- <div class="col-md-1">
          <a href="{{ route('kategori-tambah') }}" class="btn btn-block btn-primary">Tambah</a>
        </div> --}}
        <div class="col-md-11">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">DataTables</li>

          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            {{-- <div class="card-header">
              <h3 class="card-title">DataTable with minimal features & hover style</h3>
            </div> --}}
            <div class="card card-success">
                <div class="card-header">
                  <h3 class="card-title">Edit Member</h3>
                </div>
                <div class="card-body">
                <form method="post" action="{{ route('member-update', $member->id) }}">
                    @csrf
                    <label for="">Kode Member</label>
                    <input class="form-control" type="text" name="code" placeholder="Kode Member" value="{{ $member->code }}" readonly="">
                    <br>
                    <label for="">Nama Member</label>
                    <input class="form-control" type="text" name="name" placeholder="Nama Member" value="{{ $member->name }}">
                    <br>
                    <label for="">Alamat</label>
                    <input class="form-control" type="text" name="address" placeholder="Alamat" value="{{ $member->address }}">
                    <br>
                    <label for="">No Telepon</label>
                    <input class="form-control" type="text" name="telephone" placeholder="Telepon" value="{{ $member->telephone }}">
                    <br>
                  <button class="btn btn-block btn-primary col-md-1" type="submit">Edit</button>
                </form>
                </div>
                <!-- /.card-body -->
              </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

@endsection
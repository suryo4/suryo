<?php
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PengaturanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('/kategori-tambah', [KategoriController::class, 'create'])->name('kategori-tambah');
Route::post('/kategori-simpan', [KategoriController::class, 'store'])->name('kategori-simpan');
Route::get('/kategori-edit/{id}', [KategoriController::class, 'edit'])->name('kategori-edit');
Route::post('/kategori-update/{id}', [KategoriController::class, 'update'])->name('kategori-update');
Route::get('/kategori-delete/{id}', [KategoriController::class, 'destroy'])->name('kategori-delete');
Route::get('/kategori', [KategoriController::class, 'index'])->name('kategori');

Route::get('/master', [DashboardController::class, 'master'])->name('master');

Route::get('/produk', [ProductController::class, 'index'])->name('produk');
Route::get('/produk-tambah', [ProductController::class, 'create'])->name('produk-tambah');
Route::post('/produk-simpan', [ProductController::class, 'store'])->name('produk-simpan');
Route::get('/produk-edit/{id}', [ProductController::class, 'edit'])->name('produk-edit');
Route::post('/produk-update/{id}', [ProductController::class, 'update'])->name('produk-update');
Route::get('/produk-delete/{id}', [ProductController::class, 'destroy'])->name('produk-delete');
Route::get('/stok-update', [ProductController::class, 'update_stok'])->name('stok-update');
Route::post('/stok-edit', [ProductController::class, 'edit_stok'])->name('stok-edit');

Route::get('/transaction', [TransaksiController::class, 'dash'])->name('transaction');
Route::get('/transaksi', [TransaksiController::class, 'index'])->name('transaksi');
Route::get('/setting', [PengaturanController::class, 'index'])->name('setting');
Route::post('/setting-simpan', [PengaturanController::class, 'store'])->name('setting-simpan');

Route::post('simpandt', [TransaksiController::class, 'store']);
Route::get('/cetak/{id}', [TransaksiController::class, 'show']);
Route::get('/pengguna', [HomeController::class, 'pengguna']);
Route::get('/editpengguna/{id}', [HomeController::class, 'editpengguna']);
Route::get('/pengguna-delete/{id}', [HomeController::class, 'hapususer'])->name('pengguna-delete');
Route::post('/penggunainsert', [HomeController::class, 'penggunainsert']);


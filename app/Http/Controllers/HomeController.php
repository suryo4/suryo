<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        return view('dashboard');
    }
    public function editpengguna(Request $request, $id)
    {   $user=User::where('id', $id)->first();
        return view('registeredit', compact('user'));
    }

    public function pengguna(){
        $user=User::get();
        $n=1;
       return view('register',  compact('user', 'n'));
    }
        public function penggunainsert(Request $request){
            if(request('edit')){
                $u=User::where('id', $request->id)->first();
                $u->name=$request->name;
                $u->role=$request->role;
                $u->password=Hash::make($request->password);
                $u->email=$request->email;
                $u->update();

            }else{
            $iduser = User::max('id');
            $idmax = (int)$iduser + 1;
        $validasi = $this->validate($request,[
            'name' => ['required', 'string', 'max:255'],
            'role' => ['required', 'string', 'max:5'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $newuser= User::create([
            'id' => $idmax,
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'password' => Hash::make($request->password),
        ]);
        }
        return redirect('pengguna');
    }

    public function hapususer($id){
        $item = User::findOrFail($id);
        $item->delete();

        return redirect('pengguna')->with(['success' => 'Data Berhasil Dihapus']);
    }
}

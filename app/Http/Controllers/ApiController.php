<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Member;
use App\Models\Penjualan;
use App\Models\Category;
class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function membercount()
    {           $tahun = date('Y');
        // januari
        $ja = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
              ->whereMonth('tanggal_transaksi', '=', '01')
              ->sum('total');
        // feb
        $fe = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
              ->whereMonth('tanggal_transaksi', '=', '02')
              ->sum('total');
        // marert
        $ma = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
              ->whereMonth('tanggal_transaksi', '=', '03')
              ->sum('total');
        // april
        $ap = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
              ->whereMonth('tanggal_transaksi', '=', '04')
              ->sum('total');
        // mei
        $me = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
              ->whereMonth('tanggal_transaksi', '=', '05')
              ->sum('total');
        // juni
        $jun = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
              ->whereMonth('tanggal_transaksi', '=', '06')
              ->sum('total');
        // juli
        $jul = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
              ->whereMonth('tanggal_transaksi', '=', '07')
              ->sum('total');
        // agustus
        $ag = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
              ->whereMonth('tanggal_transaksi', '=', '08')
              ->sum('total');
        // september
        $sep = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
              ->whereMonth('tanggal_transaksi', '=', '09')
              ->sum('total');
        // oktober
        $ok = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
              ->whereMonth('tanggal_transaksi', '=', '10')
              ->sum('total');
         // november
         $no = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
         ->whereMonth('tanggal_transaksi', '=', '11')
         ->sum('total');
        // desember
        $de = Penjualan::select('total')->whereYear('tanggal_transaksi', '=', $tahun)
        ->whereMonth('tanggal_transaksi', '=', '12')
        ->sum('total');
        $penjualan = [
            [$ja],[$fe],[$ma],[$ap],[$me],[$jun],[$jul],[$ag],[$sep],[$ok],[$no],[$de]
        ];


         $jak = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
                ->whereMonth('tanggal_retur', '=', '01')
                ->sum('retur');
          // feb
          $fek = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
                ->whereMonth('tanggal_retur', '=', '02')
                ->sum('retur');

          // marert
          $mak = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
                ->whereMonth('tanggal_retur', '=', '03')
                ->sum('retur');
          // april
          $apk = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
                ->whereMonth('tanggal_retur', '=', '04')
                ->sum('retur');
          // mei
          $mek = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
                ->whereMonth('tanggal_retur', '=', '05')
                ->sum('retur');
          // juni
          $junk = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
                ->whereMonth('tanggal_retur', '=', '06')
                ->sum('retur');
          // juli
          $julk = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
                ->whereMonth('tanggal_retur', '=', '07')
                ->sum('retur');
          // agustus
          $agk = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
                ->whereMonth('tanggal_retur', '=', '08')
                ->sum('retur');
          // september
          $sepk = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
                ->whereMonth('tanggal_retur', '=', '09')
                ->sum('retur');
          // oktober
          $okk = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
                ->whereMonth('tanggal_retur', '=', '10')
                ->sum('retur');
           // november
           $nok = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
           ->whereMonth('tanggal_retur', '=', '11')
           ->sum('retur');
          // desember
          $dek = Penjualan::select('retur')->whereYear('tanggal_retur', '=', $tahun)
          ->whereMonth('tanggal_retur', '=', '12')
          ->sum('retur');
          $retur = [
            [$jak],[$fek],[$mak],[$apk],[$mek],[$junk],[$julk],[$agk],[$sepk],[$okk],[$nok],[$dek]
        ];
        
        
        $member=Member::count('id');
        
        // return view('pages.dashboard', compact('produk', 'transaksi', 'member', 'kategori', 'penjualan', 'retur'));
        return json_encode($member);
    }
    public function transaksicount()
    {   $transaksi=Penjualan::count('id');
        return json_encode($transaksi);
    }    
    public function produkcount()
    {   $produk=Product::count('id');
        return json_encode($produk);
    }
    public function kategoricount()
    {   $kategori=Category::count('id');
        return json_encode($kategori);
    }    
    public function productenable()
    {   $produk = Product::where('stock', '>', 0)
                            ->orderBy('product_name')
                            ->where('enable', 't')
                            ->get();
        return json_encode($produk);
    }
   
}

<?php

namespace App\Http\Controllers;
use App\Models\Transaction;
use App\Models\Product;
use App\Models\Member;
use App\Models\Penjualan;
use App\Models\Detail;
use App\Models\Setting;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dash()
    {
        $transaksi = Penjualan::all();
        return view('pages.transaksi.dashboardpenjualan',compact('transaksi'));
    }
    public function index()
    {   $member = Member::get();
        $setting = Setting::select('diskon_member')->first();
        $diskon=$setting->diskon_member;
        $produk = Product::where('stock', '>', 0)
                            ->orderBy('product_name')
                            ->where('enable', 't')
                            ->get();
        $pjmax=Penjualan::max('id');
        $pjid=(int)$pjmax+1;
        $nonota='100'.$pjid;
       
        return view('pages.transaksi.transaksi', compact('produk', 'member', 'diskon', 'nonota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $tanggal = Carbon::now();
       
        if (!empty($request->disc)) {
        $diskon=$request->disc;
    }else{
        $diskon=0;
    }

        $maxnota=Penjualan::max('id');
        $idnota= (int)$maxnota+1;
        
    foreach ($request->p_id as $index=>$p) {  
        $maxdt=Detail::max('id');
        $iddt= (int)$maxdt+1;
        $dt= new Detail;
        $dt->id=$iddt;
        $dt->penjualanid=$idnota;
        $dt->produkid=$p;
        $dt->tanggal_transaksi=$tanggal;
        $dt->qty=$request->qty[$index];
        $dt->retur=0;
        $dt->subtotal=$request->total[$index];
        $dt->save();

        $pro=Product::where('id', $p)->first();
        $stok=$pro->stock;
        $pro->stock=$stok-$request->qty[$index];
        $pro->update();
        }
        if($request->mem_id){
        $mem_id=$request->mem_id;
        }else{
            $mem_id=0;
        }
        $pj= new Penjualan;
        $pj->id= $idnota;
        $pj->nonota=$request->nonota;
        $pj->notadetailid=$iddt;
        $pj->total_item=$request->qtytot;
        $pj->diskon=$diskon;
        $pj->harga_bruto=$request->nomtot;
        $pj->nominaldiskon=$request->nomtot-$request->totalbayar;
        $pj->harga_netto=$request->totalbayar;
        $pj->via=$request->via;
        $pj->namabank=$request->bank;
        $pj->nomorbank=$request->nomor_bank;
        $pj->diterima=$request->terima;
        $pj->memberid=$mem_id;
        $pj->kembali=$request->kembali;
        $pj->total=$request->totalbayar;
        $pj->tanggal_transaksi=$tanggal;
        $pj->save();
        $setting = Setting::first();
        $penjualan=Penjualan::where('id',$idnota)->first();
        $detail=Detail::where('penjualanid', $idnota)->get();

        return view('pages.transaksi.cetaknota', compact('setting', 'penjualan', 'detail'));
        // return redirect('show/$idnota');
        // return back();
    
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $setting = Setting::first();
        $penjualan=Penjualan::where('id',$id)->first();
        $detail=Detail::where('penjualanid', $id)->get();
        return view('pages.transaksi.cetaknota', compact('setting', 'penjualan', 'detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

        public function history()
    {
         $penjualan=Penjualan::get();
         $n=1;
         return view('pages.transaksi.history', compact('penjualan', 'n'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajax(Request $request)
    {
        $id= $_GET['id'];
       
        $result=Detail::where('id', $id)->get();
 
            return view('pages.transaksi.ajaxpage')->with([

                'data'=>$result,
            ]);
        
    }

    public function read()
    {
        return 'Silahkan melakukan penccarian data';
    }

    public function all(Request $request)
    {
        $result=Product::get();

            return view('pages.transaksi.ajaxpage')->with([

                'data'=>$result,
            ]);
        
        
    }
}

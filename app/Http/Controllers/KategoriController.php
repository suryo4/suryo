<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Http\Request;



class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Category::all();
        // dd($kategori); //dump die -> mengambil nilai $kategori kemudian die yang artinya perintah selanjutnya tidak dijalankan

        return view('pages.kategori.kategori',compact('kategori')); //mengirimkan variabel $kategori ke view
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.kategori.tambah');
    }

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   $idkat=Category::max('id');
        $id=(int)$idkat + 1;
        $request->validate([
            'kode_category' => 'required|max:45',
            'name_category' => 'required|max:255'
        ]);
        Category::create([
            'id'=>$id,
            'name_category' => $request->name_category,
            'kode' => $request->kode_category,
        ]);

        return redirect()->route('kategori')->with(['success' => 'Data Berhasil Disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Category::findOrFail($id); //mengambil value
        return view('pages.kategori.edit',compact('kategori'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name_category' => 'required|max:255',
            'kode_category' => 'required|max:45'
        ]);

        Category::where('id', $id)
        ->update([
            'name_category' => $request->name_category,
            'kode' => $request->kode_category,
        ]);

        return redirect()->route('kategori')->with(['success' => 'Data Berhasil Diedit']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Category::findOrFail($id);
        $item->delete();

        return redirect()->route('kategori')->with(['success' => 'Data Berhasil Dihapus']);
    }

    
    
}

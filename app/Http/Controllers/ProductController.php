<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dash()
    {
        $produk = Product::all();
        return view('pages.produk.dashboardproduk',compact('produk'));
    }

    public function index()
    {
        $produk = Product::all();
        return view('pages.produk.produk',compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Category::all();

        return view('pages.produk.tambah',compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->gambar);
        $request->validate([
            'product_name' => 'required|max:255',
            'kategori' => 'required|max:255',
            'price' => 'required|max:255',
            'stock' => 'required|max:255',

        ]);

        // dd('asd');

        if ($img = $request->file('gambar')) {
            // dd('asc');   
            $nama_gambar = time().'.'.$img->getClientOriginalExtension();
            $image = $img->move(public_path('gambar/produk'), $nama_gambar);
            
        }

        $pro=Category::where('id', $request->kategori)->first();
        $procode=$pro->kode;
        $idpro=Product::max('id');
        $idp=(int)$idpro+1;
        // dd($request->kategori)
        Product::create([
            'id'=>$idp,
            'product_code' => $procode.'00'.$idp,
            'product_name' => $request->product_name,
            'category' => $request->kategori,
            'price' => $request->price,
            'stock' => $request->stock,
        ]);

        return redirect()->route('produk')->with(['success' => 'Data Berhasil Disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Category::all();
        $produk = Product::findOrFail($id); //mengambil value
        
        // dd($produk->kategori);
        return view('pages.produk.edit',compact('kategori','produk'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'product_code' => 'required|max:255',
            'product_name' => 'required|max:255',
            'kategori' => 'required|max:255',
            'price' => 'required|max:255',
            'stock' => 'required|max:255',
        ]);
        $pro=Category::where('id', $request->kategori)->first();
        $procode=$pro->kode;
        $gambar = $request->gambar;

        $data = [
            'product_code' => $procode.'00'.$id,
            'product_name' => $request->product_name,
            'category' => $request->kategori,
            'price' => $request->price,
            'stock' => $request->stock
        ];
        
        if($gambar) {
            if ($img = $request->file('gambar')) {
                // dd('asc');   
                $produk = Product::where('id', $id)->first();

                $path = public_path().'/gambar/produk/';
                $file_old = $path.$produk->image;
                unlink($file_old);
                
                $nama_gambar = time().'.'.$img->getClientOriginalExtension();
                $image = $img->move(public_path('gambar/produk'), $nama_gambar);
                
            }

            $data ['image'] = $nama_gambar;
        }

        Product::where('id', $id)
            ->update($data);
        
        return redirect()->route('produk')->with(['success' => 'Data Berhasil Diedit']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Product::findOrFail($id);

        // $path = public_path().'/gambar/produk/';
        // $file_old = $path.$item->image;
        // unlink($file_old);

        $item->delete();

        return redirect()->route('produk')->with(['success' => 'Data Berhasil Dihapus']);
    }

    public function update_stok(){
        
        $product = Product::all();
        return view('pages.produk.stok',compact('product'));
    }

    public function edit_stok(Request $request){
       
        $request->validate([
            'product_id' => 'required|max:255',
            'stock' => 'required|min:1',
        ]);
        $p=Product::where('id', $request->product_id)->first();
        $p->stock=$request->stock;
        $p->update();
        return redirect(route('produk'));
    }
}


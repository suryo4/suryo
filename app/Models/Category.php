<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    protected $fillable = ['name_category', 'kode', 'id'];

    public function produk()
    {
        return $this->hasMany(Product::class, 'category', 'id');
    }
}

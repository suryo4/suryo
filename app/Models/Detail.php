<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = 'penjualandetail';
    protected $fillable = ['id', 'penjualanid','produkid','qty','retur','subtotal'];

        public function produk()
    {
    // return $this->hasMany(Product::class, 'id', 'produkid');
    	return $this->belongsTo(Product::class, 'produkid', 'id');
}
}

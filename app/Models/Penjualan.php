<?php
namespace App\Models;
use App\Models\Member;
use App\Models\Detail;


use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    protected $table = 'penjualan';
    protected $fillable = ['id', 'nonota','notadetailid','total_item','diskon','harga_bruto', 'harga_netto', 'bayar', 'diterima', 'kembali', 'tanggal_transaksi', 'tanggal_retur'];
    public function member()
    {
    	return $this->belongsTo(Member::class, 'memberid', 'id');
}
    public function detail()
    {
    	return $this->belongsTo(Detail::class, 'penjualanid', 'id');
}
}

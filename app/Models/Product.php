<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['id', 'product_code','product_name','category','price','stock'];

    public function kategori()
    {
        return $this->belongsTo(Category::class, 'category', 'id');
    }
}
